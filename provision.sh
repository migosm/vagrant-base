#!/bin/bash

add-apt-repository -y ppa:ondrej/php
apt-get update
apt-get install -y nginx php7.2-fpm php-pear php7.2-mbstring mysql-server-5.7 mysql-server-core-5.7 php5.6-fpm php5.6-mcrypt php5.6-mbstring curl
apt-get purge -y apache2
cp -f /vagrant/configs/php/5.6/php-fpm-www.conf /etc/php/5.6/fpm/pool.d/www.conf
cp -f /vagrant/configs/php/7.2/php-fpm-www.conf /etc/php/7.2/fpm/pool.d/www.conf
curl -o /home/vagrant/.ssh/authorized_keys https://raw.githubusercontent.com/hashicorp/vagrant/master/keys/vagrant.pub

